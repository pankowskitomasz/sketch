import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServicesRoutingModule } from './services-routing.module';
import { ServicesComponent } from './services/services.component';
import { ServicesS1Component } from './services-s1/services-s1.component';
import { ServicesS2Component } from './services-s2/services-s2.component';


@NgModule({
  declarations: [
    ServicesComponent,
    ServicesS1Component,
    ServicesS2Component
  ],
  imports: [
    CommonModule,
    ServicesRoutingModule
  ]
})
export class ServicesModule { }
