import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesS2Component } from './courses-s2.component';

describe('CoursesS2Component', () => {
  let component: CoursesS2Component;
  let fixture: ComponentFixture<CoursesS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoursesS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
