import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesS1Component } from './courses-s1.component';

describe('CoursesS1Component', () => {
  let component: CoursesS1Component;
  let fixture: ComponentFixture<CoursesS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoursesS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
