import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoursesRoutingModule } from './courses-routing.module';
import { CoursesComponent } from './courses/courses.component';
import { CoursesS1Component } from './courses-s1/courses-s1.component';
import { CoursesS2Component } from './courses-s2/courses-s2.component';


@NgModule({
  declarations: [
    CoursesComponent,
    CoursesS1Component,
    CoursesS2Component
  ],
  imports: [
    CommonModule,
    CoursesRoutingModule
  ]
})
export class CoursesModule { }
